import React from 'react';

function GifList(props) {
  const { gifs, loading, getRandomGif } = props;
  if (loading) {
    return (
      <div className="loading-container">
        <p className="loading">Cargando...</p>
      </div>
    );
  }

  return (
    <div className="gif-list">
      <div>
        {
          gifs.length > 0
            ? gifs.map(gif => {
                return <img key={gif.id} src={gif.images.original.url} />
              })
            : 'No hay GIFS :('
        }
      </div>
      <div className="load-more-container">
        <button className="load-more" onClick={getRandomGif} >AGREGAR UNO RANDOM ⚡️</button>
      </div>
    </div>
  )
}

export default GifList;
