import React from 'react';

function Gif(props) {
  const { source } = props;
  return (
    <div className="gif-container">
      <img className="gif" src={source} />
    </div>
  );
}

export default Gif;