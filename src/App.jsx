import React from 'react';
import Header from './Components/Header';
import  './App.css';
import GifList from './Components/GifList';
import SearchBox from './Components/Searchbox';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gifs: [],
      loading: true,
      search: '',
    }
  }

  componentDidMount() {
    const url = 'http://api.giphy.com/v1/gifs/trending?api_key=49uWtbSLzAkgT9VruGNerOCmDo5ke5pt&limit=5';
    fetch(url).then(res => res.json()).then(response => {
      const { data } = response;

      this.setState({
        gifs: data,
        loading: false,
      })
    })
  }

  handleInputChange = (event) => {
    this.setState({
      search: event.target.value,
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const query = this.state.search;
    if (query === '') return;

    const url = `http://api.giphy.com/v1/gifs/search?api_key=49uWtbSLzAkgT9VruGNerOCmDo5ke5pt&q=${query}`

    fetch(url).then(res => res.json()).then(response => {
      const { data } = response;

      this.setState({
        gifs: data,
        loading: false,
        search: '',
      })
    })
  }

  getRandomGif = () => {
    const url = 'http://api.giphy.com/v1/gifs/random?api_key=49uWtbSLzAkgT9VruGNerOCmDo5ke5pt';
    fetch(url).then(res => res.json()).then(jsonResponse => {
      // const id = jsonResponse.data.id;
      // const image_original_url = jsonResponse.data.image_original_url;
      // const status = jsonResponse.data.status;
      const { data } = jsonResponse;

      this.setState({
        gifs: this.state.gifs.concat([data]),
      })
    })
  }

  render() {
    return (
      <div className="App">
        <div className="container">
          <Header />
          <SearchBox
            value={this.state.search}
            onChange={this.handleInputChange}
            onSubmit={this.handleSubmit}
          />
          <GifList
            gifs={this.state.gifs}
            getRandomGif={this.getRandomGif}
          />
        </div>
      </div>
    );
  }
}

export default App;
